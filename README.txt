
This module was born out of the need for a simple module that supports the Domain Access module and offers SSO between the enabled domains (via a small config page tweak/addon on the Domain Acess domain settings page).

Currently it just works for all domains, the config page still needs to be added, but we'r first focussing on a couple other ideas, like:

TODO

See the TODO markers in the code + these:

- Custom domain list entry: a simple way to add a custom domain to the scope. The idea is sometimes you would like to enable SSO across a site that is not part of the domain setup, so you could simply install the services module and configure that, but maybe we can put something together. Things like syncing users will not be part of this project, so something to think about.
- Permissions? (able to login to domain xy and not z. (we currently use the domains a user is assigned to).
- (note: a cleaner way then we currently already implement is almost not possible now, only thing we could do is influence the session table ourselves.) Clean login url, maybe even ajax, with a template with absolute minimum of stuff. no html, no nothing, also no redirects. (but that creates new problems, like clicking away from the page == stop execute logouts) (other modules suffer these kinds of problems, let's not do the same).
- Maybe some config options for commented out functionality.
- Support for login destination and related modules.
- We might want to refactor some code, like the get destination code. In domain_sso_user_logout() we call domain_sso_get_destination() and after this unset the destination. We need to investigate a bit more into this, see if we can refactor domain_sso_get_destination() and unset all destinations here. The idea here is that the last page should trigger any destination, so a form or GET destination should be unset anyway and the destination should be passed via the url, so the final page can call it. This last bit already works, so we now only need to combine some code.
  UPDATE: i have moved the unset() to the domain_sso_get_destination() function, and added an unset() for the form redirect. Still need to test this last bit.
- Add a configuration page with some minimal options. An 'Execute login/logout hooks' option would be a neat idea to have. (maybe per domain).
- Something to make the login form only work over ssl (per domain?) (to prevent 'issues' i would make all sites use ssl if i can choose).



7.x

based on 1 domain per language (so a pure multilangual i18n site with domain names per language)
https://drupal.org/project/i18n_sso

requires the same domain (also 6.x available)
https://drupal.org/project/bakery


6.x

has (known) issues (also 5.x available)
https://drupal.org/project/sso

marked insecure
https://drupal.org/project/singlesignon

5.x
https://drupal.org/project/fierce_sso






https://drupal.org/project/services_sso_client
https://drupal.org/project/services_sso_server_helper


